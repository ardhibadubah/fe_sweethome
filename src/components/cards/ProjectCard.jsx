import axios from 'axios';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { HeartIcons } from '../../assets';
import { rupiahFormatter } from '../../utility/number';

const ProjectCard = (props) => {
  const data = props.data;
  const isLogin = localStorage.getItem('token');

  const toggleFavorite = () => {
    axios({
      method: data.isFavorite ? 'delete' : 'post',
      url: `${process.env.REACT_APP_BASE_API}favorite/${data.id}`,
      headers: {
        Authorization: localStorage.getItem('token'),
      },
    }).then(() => {
      props.getShowcase({ page: 1 });
    });
  };

  return (
    <div className='col-12 col-md-6'>
      <div className='ProjectCard card text-decoration-none text-dark shadow-sm'>
        {isLogin && (
          <div className='icon-favorite' onClick={() => toggleFavorite()}>
            <HeartIcons
              fill={`${data.isFavorite ? '#DD5571' : 'white'}`}
              stroke={`${!data.isFavorite && '#214457'}`}
            />
          </div>
        )}
        <Link to={`/project/${data.id}`}>
          <Card.Img variant='top' src={data.gallery[0].picture} />
          <Card.Body>
            <div className='d-flex justify-content-between'>
              <Card.Title className='fw-bold text-truncate mb-0'>
                {data.name}
              </Card.Title>
              {data.project && (
                <Card.Text className='estimated-time mb-0'>
                  {data.project.totalDuration} weeks
                </Card.Text>
              )}
            </div>
            <div className='d-flex justify-content-between'>
              {data.project ? (
                <Card.Text className='text-truncate mb-0'>
                  {data.project.appointment.address}
                </Card.Text>
              ) : (
                <Card.Text className='mb-0'></Card.Text>
              )}
              {data.project && (
                <Card.Text>
                  {rupiahFormatter(parseInt(data.project.totalPrice)).replace(
                    ',00',
                    ''
                  )}
                </Card.Text>
              )}
            </div>
          </Card.Body>
        </Link>
      </div>
    </div>
  );
};

export default ProjectCard;
