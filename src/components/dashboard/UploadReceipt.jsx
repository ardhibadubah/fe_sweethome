import {Button} from 'react-bootstrap';
import {useState} from 'react';
import PaymentModal from './PaymentModal';
import CancelModal from './CanceLModal';

const UploadReceipt = (props)=>{
   const [paymentShow, setPaymentShow] = useState(false);
   const [cancelShow, setCancelShow] = useState(false);

   return(
      <div className="uploadReceipt text-end">
         <div className="buttonUpload">
            <Button variant="primary" className="px-5" onClick={() => setPaymentShow(true)}>
               Upload Receipt
            </Button>
         </div>
         <div className="cancelRequest pt-2">
         <span className="text-ash" onClick={() => setCancelShow(true)}>
            Request Cancellation
         </span>
         </div>
         <div className="showModal">
            <PaymentModal
               id={props.projectId}
               show={paymentShow}
               onHide={() => setPaymentShow(false)}
               reload={() => props.reload()}
            />
            <CancelModal
               id={props.projectId}
               code={props.id}
               show={cancelShow}
               onHide={() => setCancelShow(false)}
               reload={() => props.reload()}
            />
         </div>
      </div>
   )
}

export default UploadReceipt;