import { FavouriteCard, CardLoading } from "../../components";
import { useState, useEffect } from "react";
import axios from "axios";
import EmptySection from "../blank/EmptySection";

const FavouriteSection = () => {
  const token = localStorage.getItem("token");
  const [favourite, setFavourite] = useState({
    loading: true,
    data: false,
    error: false,
  });

  const getFavourite = () => {
    axios({
      method: "get",
      url: `${process.env.REACT_APP_BASE_API}favorite`,
      headers: {
        Authorization: token,
      },
    })
      .then((response) => {
        setFavourite({
          loading: false,
          data: response.data,
          error: false,
        });
      })
      .catch((error) => {
        setFavourite({
          loading: false,
          data: null,
          error: error.message,
        });
      });
  };

  useEffect(() => {
    getFavourite();
    // eslint-disable-next-line
  }, []);

  const items = favourite.data;

  return (
    <div className="favouriteSection row g-5 mb-5 px-5">
      {favourite.data && favourite.data.length <= 0 && <EmptySection quote='favourites' />}
      {favourite.loading ? (
        <CardLoading />
      ) : (
        <FavouriteCard data={items} reload={() => getFavourite()} />
      )}
    </div>
  );
};

export default FavouriteSection;
