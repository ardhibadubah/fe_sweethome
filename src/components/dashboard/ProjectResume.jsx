import {Row, Col, Accordion } from "react-bootstrap";
import { useState, useEffect } from "react";
import UploadReceipt from "./UploadReceipt";
import PaymentSuccess from "./PaymentSuccess";
import axios from "axios";
import moment from "moment";
import { Loading } from "../../components";
import { color, projectStatus } from "../../utility/status";
import { rupiahFormatter } from "../../utility/number";
import EmptySection from "../blank/EmptySection";

const ProjectResume = () => {
  const token = localStorage.getItem("token");
  const [project, setProject] = useState({
    loading: true,
    data: false,
    error: false,
  });

  const getProject =()=>{
     axios({
       method: "get",
       url: `${process.env.REACT_APP_BASE_API}project`,
       headers: {
         Authorization: token,
       },
     })
       .then((response) => {
         setProject({
           loading: false,
           data: response.data,
           error: false,
         });
       })
       .catch((error) => {
         setProject({
           loading: false,
           data: null,
           error: error.message,
         });
       });
  }

  useEffect(() => {
     getProject();
    // eslint-disable-next-line
  }, []);

  const data = project.data;

  return (
    <>
      {project.data && project.data.length <= 0 && <EmptySection quote='project'/>}
      {project.loading ? (
        <Loading />
      ) : (
        <div className="projectResume">
          <Accordion>
            {data.map((data, index) => {
              return (
                <Accordion.Item
                  className={`border-${color(
                    data.status
                  )} shadow p-3 mb-5 bg-body`}
                  eventKey={index}
                  key={index}
                >
                  <Accordion.Header>
                  <Row className="d-flex w-100">
                      <Row className="align-items-center mb-2">
                        <Col md={6} xs={12}>
                          <div
                            className={`status badge px-3 fs-6 rounded-pill bg-${color(
                              data.status
                            )}`}
                          >
                            {projectStatus(data.status)}
                          </div>
                        </Col>
                        <Col>
                          <div className="date text-ash">
                            {`Created on ${moment(data.completedAt).format(
                              "LL"
                            )}`}
                          </div>
                        </Col>
                      </Row>
                      <Row className="d-flex pt-3">
                        <Col xs={12}>
                           <div className="d-flex pt-3">
                              <p className="mb-0 pe-2 fw-bold">
                                 {data.projectDetail[0]?.projectType.name}
                              </p>
                              {data.projectDetail.length > 1 && (
                                 <>
                                    <span className="text-ash">+</span>
                                    <p className="mb-0 ps-2 text-ash"> {`${data.projectDetail.length - 1} packages more`}</p>
                                 </>
                              )}
                           </div>
                        </Col>
                      </Row>
                    </Row>
                  </Accordion.Header>
                  <Accordion.Body>
                    <div className="detail w-100">
                      <div className="projtDetail">
                        {data.status === 1 ? (
                          <div className="transfer px-5 py-2 col-6 mb-4 bg-ash">
                            Transfer to xxxxxxx
                          </div>
                        ) : (
                          <div></div>
                        )}
                        <div className="details row">
                          <div className="projectDetail col-8">
                            <div className="row">
                              <div className="col-3">
                                <div className="text-ash">Project ID</div>
                                <div className="fw-bold">{data.code}</div>
                              </div>
                              <div className="col-3">
                                <div className="text-ash">Building Type</div>
                                <div className="fw-bold">
                                  {data.appointment.buildingType.name}
                                </div>
                              </div>
                              <div className="col-6">
                                <div className="text-ash">
                                  Related Appointment
                                </div>
                                <div className="fw-bold text-primary">
                                  {data.appointment.code}
                                </div>
                                <div className="text-primary">
                                  <span className="mb-0 pe-1">
                                    {data.appointment.appointmentDate}
                                  </span>
                                  <span className="vr"></span>
                                  <span className="mb-0 ps-1">
                                    {data.appointment.timeslot.time}
                                  </span>
                                </div>
                              </div>
                              <div className="row pt-4">
                                <div className="text-ash">Address</div>
                                <div className="fw-bold">
                                  {data.appointment.address}
                                </div>
                              </div>
                            </div>
                            {/* upload receipt component here */}
                          </div>
                          <div className="col-4">
                            {(() => {
                              switch (data.status) {
                                case -1:
                                  return (
                                    <PaymentSuccess
                                      complete={data.completedAt}
                                      confirm={data.uploadReceipt}
                                      note={data.noteUploadReceipt}
                                    />
                                  );
                                case 1:
                                  return (
                                    <UploadReceipt
                                      id={data.code}
                                      projectId={data.id}
                                      reload={() => getProject()}
                                    />
                                  );
                                case 2:
                                  return (
                                    <PaymentSuccess
                                      complete={data.completedAt}
                                      confirm={data.uploadReceipt}
                                      note={data.noteUploadReceipt}
                                    />
                                  );
                                case 3:
                                  return (
                                    <PaymentSuccess
                                      complete={data.completedAt}
                                      confirm={data.uploadReceipt}
                                      note={data.noteUploadReceipt}
                                    />
                                  );
                                default:
                                  return null;
                              }
                            })()}
                          </div>
                          <div className="billing mt-5">
                            <div className="row pb-2 border-bottom border-ash">
                              <div className="col text-ash">Project Type</div>
                              <div className="col text-ash">Area</div>
                              <div className="col text-ash">Work Duration</div>
                              <div className="col text-ash">Total</div>
                            </div>
                            {data.projectDetail.map((data, index) => {
                              return (
                                <div
                                  className="row pt-3 pb-3 border-bottom border-ash"
                                  key={index}
                                >
                                  <div className="col fw-bold">
                                    {data.projectType.name}
                                  </div>
                                  <div className="col">{data.section.name}</div>
                                  <div className="col">{`${data.workDuration} Weeks`}</div>
                                  <div className="col fw-bold">
                                    {rupiahFormatter(
                                      parseInt(data.price)
                                    ).replace(",00", "")}
                                  </div>
                                </div>
                              );
                            })}
                            {/* <div className="row pt-3 pb-3 border-bottom border-ash">
                                    <div className="col fw-bold">Carpentry Works</div>
                                    <div className="col">Kitchen</div>
                                    <div className="col">2 Weeks</div>
                                    <div className="col fw-bold">Rp.57.000.000</div>
                                    </div> */}
                            <div className="row pt-3 pb-3 bg-ash">
                              <div className="col-6 fw-bold text-end">
                                Duration
                              </div>
                              <div className="col-2">{`${data.totalDuration} weeks`}</div>
                              <div className="col-1 fw-bold text-end">
                                Total
                              </div>
                              <div className="col-3">
                                {rupiahFormatter(
                                  parseInt(data.totalPrice)
                                ).replace(",00", "")}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Accordion.Body>
                </Accordion.Item>
              );
            })}
          </Accordion>
        </div>
      )}
    </>
  );
};

export default ProjectResume;
