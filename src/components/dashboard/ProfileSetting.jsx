import {Modal, Form, Button, Col, Row} from 'react-bootstrap';
import {useState} from 'react';
import Swal from 'sweetalert2';
import axios from 'axios';

const ProfileSetting = (props)=>{
   
   const [profile, setProfile] = useState({
      firstName:'',
      lastName:'',
      email:'',
      phone:'',
   })
   const [selectedFile, setSelectedFile] = useState();

	const changeHandler = (event) => {
		setSelectedFile(event.target.files[0]);
	};

   const data = new FormData()
   data.append('picture', selectedFile);
   data.append('firstName', profile.firstName);
   data.append('lastName', profile.lastName);
   data.append('email', profile.email);
   data.append('phone', profile.phone)

   const updateProfile = () =>{
      axios({
            method: 'post',
            url: `${process.env.REACT_APP_BASE_API}user/profile`,
            headers: {
            Authorization: localStorage.getItem('token'),
            },
            data
         })
            .then((response) => {
               console.log(response)
            Swal.fire({
               icon: 'success',
               title: 'SUCCESS',
               text: 'Your profile has been updated',
            });
            window.location.reload(false)
            })
            .catch((error) => {
            Swal.fire({
               icon: 'error',
               title: 'Oops...',
               text: error.message,
            });
         });
   }

   const handleSubmit =(e)=>{
      e.preventDefault();
      updateProfile();
      props.onHide();
   };

   return(
      <Modal
         {...props}
         size="md"
         aria-labelledby="contained-modal-title-vcenter"
         centered
      >
         <Modal.Header className="border-0" closeButton>
            <Modal.Title id="contained-modal-title-vcenter" className="fw-bold">
               <div className="fw-bold">
                  Edit Profile
               </div>
            </Modal.Title>
         </Modal.Header>
         <Modal.Body>
            <div className="photoProfile text-center pb-2">
               <img 
                  className='imageProfileSet img-fluid rounded-circle' 
                  src={props.image || 'https://www.adinata.org/content/about/structure/1_1620680872_user.png'} 
                  alt='profile'
               />
            </div>
            <Form onSubmit={handleSubmit}>
               <Form.Label>Upload Profile Picture</Form.Label>
               <Form.Control 
                  type="file" 
                  placeholder="Masukan Project Id" 
                  onChange={changeHandler}
               />
               <Row>
                  <Col> 
                     <Form.Label className="pt-2">First Name</Form.Label>
                     <Form.Control
                        placeholder="First name"
                        type="text"
                        value={profile.firstName}
                        onChange={(e)=>setProfile({...profile, firstName: e.target.value})} 
                     />
                  </Col>
                  <Col>
                     <Form.Label className="pt-2">Last Name</Form.Label>
                     <Form.Control 
                        placeholder="Last name" 
                        type="text"
                        value={profile.lastName}
                        onChange={(e)=>setProfile({...profile, lastName: e.target.value})}
                     />
                  </Col>
               </Row>
               <Form.Label className="pt-2">email</Form.Label>
               <Form.Control
                  type="email"
                  placeholder="Enter email"
                  value={profile.email}
                  onChange={(e)=>setProfile({...profile, email: e.target.value})}
               />
               <Form.Label className="pt-2">Phone Number</Form.Label>
               <Form.Control
                  type="number"
                  placeholder="Enter your phone number"
                  value={profile.phone}
                  onChange={(e)=>setProfile({...profile, phone: e.target.value})}
               />
               <div className="button text-center">
                  <Button 
                     // onClick={props.onHide} 
                     type="submit"
                     variant="secondary" 
                     className="text-dark fw-bold px-5 my-4"
                  >
                     Save Change
                  </Button>
               </div>
            </Form>
         </Modal.Body>
      </Modal>
   )
}
export default ProfileSetting;