import { Loading } from '../../components';
import icon from '../../assets/icons/svg/plus.svg';
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getUser } from '../../redux/actionCreators/userAction';
import { Row, Col } from 'react-bootstrap';
import ProfileSetting from './ProfileSetting';
import { useNavigate } from 'react-router-dom';

const Profile = () => {
  const [showSetting, setShowSetting] = useState(false);
  const user = useSelector((state) => state.user.user);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const direct = () => {
    navigate('/appointment');
  };

  useEffect(() => {
    dispatch(getUser());
    // eslint-disable-next-line
  }, []);

  return (
    <>
      {user.loading ? (
        <Loading />
      ) : (
        <div className='profileSection'>
          <div className='d-flex flex-column mx-auto'>
            <div className='welcomeText'>
              <h2 className='serif fw-bold py-4'>Welcome Home!</h2>
            </div>
            <Row className='profileContent align-items-center'>
              <Col lg={6} xs={12} className='userProfile d-flex mb-3'>
                <div className='imageSection'>
                  <img
                    className='imageProfile img-fluid rounded-circle'
                    src={
                      user.data.picture ||
                      'https://www.adinata.org/content/about/structure/1_1620680872_user.png'
                    }
                    alt='profile'
                  />
                </div>
                <div className='detailProfile ps-4'>
                  <h4 className='profileName mb-0 fw-bold'>{`${user.data.firstName} ${user.data.lastName}`}</h4>
                  <p className='profileEmail mb-0'>{user.data.email}</p>
                  <p className='phoneNumber mb-0'>{user.data.phone}</p>
                  <button
                    className='border-0 bg-transparent p-0'
                    onClick={() => setShowSetting(true)}>
                    Setting
                  </button>
                </div>
              </Col>
              <Col className='buttonSection'>
                <button
                  type='button'
                  className='btn btn-primary px-3 py-2 shadow-sm'>
                  <div className='buttonContent align-items-center'>
                    <img src={icon} alt='button icon' />
                    <span className='ps-4 fw-bold' onClick={() => direct()}>
                      Create Appointement
                    </span>
                  </div>
                </button>
              </Col>
            </Row>
          </div>
          <div className='showSetting'>
            <ProfileSetting
              image={user.data.picture}
              show={showSetting}
              onHide={() => setShowSetting(false)}
            />
          </div>
        </div>
      )}
    </>
  );
};

export default Profile;
