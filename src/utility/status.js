import UploadReceipt from "../components/dashboard/UploadReceipt";
import PaymentSuccess from "../components/dashboard/PaymentSuccess";

export const color = (id)=>{
   switch(id) {
      case -1:
         return 'danger';
      case 1:
         return 'secondary';
      case 2:
         return 'info';
      case 3:
         return 'success';
      default:
         return 'primary';
   }
}

export const appointmentStatus = (id) =>{
   switch(id) {
      case -1:
         return 'Canceled';
      case 1:
         return 'Waiting Approvement';
      case 2:
         return 'Scheduled';
      case 3:
         return 'Done';
      default:
         return '';
   }
}

export const projectStatus = (id) =>{
   switch(id) {
      case -1:
         return 'Canceled';
      case 1:
         return 'Waiting Payment';
      case 2:
         return 'On Going';
      case 3:
         return 'Done';
      default:
         return '';
   }
}

export const paymentStatus = (id)=>{
   switch(id) {
      case -1:
         return <PaymentSuccess/>;
      case 1:
         return <UploadReceipt/>;
      case 2:
         return <PaymentSuccess/>;
      case 3:
         return <PaymentSuccess/>;
      default:
         return null;
   }
}