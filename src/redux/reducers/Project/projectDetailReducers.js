import {
   GET_PROJECT_DETAIL_REQUEST,
   GET_PROJECT_DETAIL_SUCCES,
   GET_PROJECT_DETAIL_ERROR,
 } from "../../constants";
 
 const initialState = {
   project: false,
   loading: true,
   error: false,
 };
 
const projectDetailReducer = (state = initialState, action) => {
   const {type, payload}= action;
   switch (type) {
     case GET_PROJECT_DETAIL_REQUEST:
       return {
         ...state,
         project: payload.project,
         loading: payload.loading,
         error: payload.error,
       };
     case GET_PROJECT_DETAIL_SUCCES:
       return {
         ...state,
         project: payload.project,
         loading: payload.loading,
         error: payload.error,
       };
     case GET_PROJECT_DETAIL_ERROR:
       return {
         ...state,
         project: payload.project,
         loading: payload.loading,
         error: payload.error,
       };
     default:
       return state;
   }
 };

 export default projectDetailReducer;